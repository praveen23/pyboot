from pyboot2.exception import InvalidValueException

VALID_ORDER_BY_VALUES = ["asc", "desc"]


class APIFilters:
    def __init__(self, filter_str, default=None):
        self.__filters = {}

        self.parse(filter_str, default)

    def parse(self, filter_str, default=None):
        if not filter_str: filter_str = default
        if not filter_str: return

        segments = filter_str.split(";")
        for segment in segments:
            segment = segment.strip()
            if not segment:
                continue
            parts = segment.split(":", 1)
            if len(parts) < 2:
                raise InvalidValueException("Invalid filters format")
            key = parts[0]
            values = parts[1]
            if not key:
                raise InvalidValueException("Invalid filters format")
            if not values:
                continue
            self.__filters[key] = [value.strip() for value in values.split(",")]

    def get(self, key, default=None):
        if key in self.__filters:
            return self.__filters[key][0]
        else:
            return default

    def get_all(self, key, default=None):
        if key in self.__filters:
            return self.__filters[key]
        else:
            return default

    def has(self, key):
        return key in self.__filters

    def keys(self):
        return self.__filters.keys()

    def __bool__(self):
        return True if self.__filters else False


class APIOrderBy:
    def __init__(self, order_by_str, default=None):
        self.__order_by_clauses = []

        self.parse(order_by_str, default=default)

    def parse(self, order_by_str, default=None):
        if not order_by_str: order_by_str = default
        if not order_by_str: return

        segments = order_by_str.split(";")
        for segment in segments:
            segment = segment.strip()
            if not segment:
                continue
            parts = segment.split(":", 1)
            if len(parts) < 2:
                raise InvalidValueException("Invalid order-by format")
            key = parts[0]
            value = parts[1]
            if not key:
                raise InvalidValueException("Invalid order-by format")
            if not value:
                continue
            elif value in VALID_ORDER_BY_VALUES:
                self.__order_by_clauses.append({key: value})

    def get(self, key):
        for order_by in self.order_by_list:
            if key in order_by:
                return order_by[key]

    def has(self, key):
        for order_by in self.order_by_list:
            if key in order_by:
                return True
        return False

    def raw(self):
        return self.__order_by_clauses

    def __bool__(self):
        return True if self.__order_by_clauses else False
