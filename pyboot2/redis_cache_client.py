from flask.ext.cache import Cache, null_control
import inspect


class RedisCache(Cache):
    """redis key design -
        instance methods - uses all instance variables and function params
        static methods - uses just function params
        NOTE - does not support caching when objects passed as params"""

    def __init__(self, flask, with_jinja2_ext=True, config=None):
        super().__init__(app=flask, config=config)

    def _memoize_kwargs_to_args(self, f, *args, **kwargs):
        result1 = super()._memoize_kwargs_to_args(f, *args, **kwargs)
        return (result1[0][1:], result1[1]) if 'object' in str(result1[0][0]) else result1

    def _memoize_version(self, f, args=None,
                         reset=False, delete=False, timeout=None):
        """
        Updates the hash version associated with a memoized function or method.
        """
        fname, instance_fname = RedisCache.function_namespace(f, args=args)
        version_key = self._memvname(fname)
        fetch_keys = [version_key]

        if instance_fname:
            instance_version_key = self._memvname(instance_fname)
            fetch_keys.append(instance_version_key)

        # Only delete the per-instance version key or per-function version
        # key but not both.
        if delete:
            self.cache.delete_many(fetch_keys[-1])
            return fname, None

        version_data_list = list(self.cache.get_many(*fetch_keys))
        dirty = False

        if version_data_list[0] is None:
            version_data_list[0] = self._memoize_make_version_hash()
            dirty = True

        if instance_fname and version_data_list[1] is None:
            version_data_list[1] = self._memoize_make_version_hash()
            dirty = True

        # Only reset the per-instance version or the per-function version
        # but not both.
        if reset:
            fetch_keys = fetch_keys[-1:]
            version_data_list = [self._memoize_make_version_hash()]
            dirty = True

        if dirty:
            self.cache.set_many(dict(zip(fetch_keys, version_data_list)),
                                timeout=timeout)

        return fname, ''.join(version_data_list)

    @staticmethod
    def function_namespace(f, args=None):
        """
        Attempts to returns unique namespace for function
        """
        m_args = inspect.getargspec(f)[0]
        instance_token = None

        instance_self = getattr(f, '__self__', None)
        if instance_self and not inspect.isclass(instance_self):
            instance_token = repr(f.__self__)
        elif m_args and m_args[0] == 'self' and args:
            instance_token = repr(args[0].__dict__)

        module = f.__module__

        if hasattr(f, '__qualname__'):
            name = f.__qualname__
        else:
            klass = getattr(f, '__self__', None)

            if klass and not inspect.isclass(klass):
                klass = klass.__class__

            if not klass:
                klass = getattr(f, 'im_class', None)

            if not klass:
                if m_args and args:
                    if m_args[0] == 'self':
                        klass = args[0].__class__
                    elif m_args[0] == 'cls':
                        klass = args[0]

            if klass:
                name = klass.__name__ + '.' + f.__name__
            else:
                name = f.__name__
        ns = '.'.join((module, name))
        ns = ns.translate(*null_control)

        if instance_token:
            ins = '.'.join((module, name, instance_token))
            ins = ins.translate(*null_control)
        else:
            ins = None

        return ns, ins
