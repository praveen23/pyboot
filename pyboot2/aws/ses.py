import datetime
import logging
from contextlib import contextmanager
from email.mime.application import MIMEApplication
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from smtpd import COMMASPACE

import boto3

from pyboot2.util import DateTimeUtil

BODY_TYPE_PLAIN = "plain"
BODY_TYPE_HTML = "html"


class AmazonSESClient:
    def __init__(self, access_key_id, access_key_secret, region_name, enabled=True):
        self.__client = boto3.client('ses', aws_access_key_id=access_key_id, aws_secret_access_key=access_key_secret,
                                     region_name=region_name)
        self.__enabled = enabled

    def send_email(self, sender, to_addresses, subject, body, body_type=BODY_TYPE_PLAIN, reply_addresses=None,
                   cc_addresses=None, bcc_addresses=None, attachments=None):
        if not self.__enabled:
            logging.info("EmailClient is disabled, not sending email")
            return

        message = MIMEMultipart()
        message.set_charset('utf-8')

        message['Subject'] = subject
        message['From'] = sender
        message['To'] = self.__convert_to_strings(to_addresses)
        if cc_addresses: message['CC'] = self.__convert_to_strings(cc_addresses)
        if bcc_addresses: message['BCC'] = self.__convert_to_strings(bcc_addresses)

        if reply_addresses:
            message['Reply-To'] = self.__convert_to_strings(reply_addresses)

        message.attach(MIMEText(body, body_type, "utf-8"))
        if attachments and len(attachments) > 0:
            for attachment in attachments:
                if attachment["type"] == "file":
                    part = MIMEApplication(attachment["file"].read())
                elif attachment["type"] == "filename":
                    with open(attachment["filename"], 'rb') as f:
                        part = MIMEApplication(f.read())
                part.add_header('Content-Disposition', 'attachment', filename=attachment["name"])
                message.attach(part)

        receipients = []
        if to_addresses and isinstance(to_addresses, list):
            receipients += to_addresses
        elif isinstance(to_addresses, str):
            receipients.append(to_addresses)

        if cc_addresses and isinstance(cc_addresses, list):
            receipients += cc_addresses
        elif isinstance(cc_addresses, str):
            receipients.append(cc_addresses)

        if bcc_addresses and isinstance(bcc_addresses, list):
            receipients += bcc_addresses
        elif isinstance(bcc_addresses, str):
            receipients.append(bcc_addresses)

        # logging.debug(message.as_string())

        return self.__client.send_raw_email(RawMessage={"Data": message.as_string()}, Source=message["From"],
                                            Destinations=receipients)

    def __convert_to_strings(self, list_of_strs):
        if isinstance(list_of_strs, (list, tuple)):
            result = COMMASPACE.join(list_of_strs)
        else:
            result = list_of_strs
        return result


class AmazonSESConf:
    def __init__(self, access_key_id=None, access_key_secret=None, region_name=None, enabled=True):
        self.access_key_id = access_key_id
        self.access_key_secret = access_key_secret
        self.region_name = region_name
        self.enabled = enabled

    @contextmanager
    def get_client(self):
        start_time = datetime.datetime.now()
        client = AmazonSESClient(self.access_key_id, self.access_key_secret, self.region_name, self.enabled)

        try:
            yield client  # type: AmazonSESClient
        finally:
            logging.debug(
                f"Email client transaction time: {DateTimeUtil.diff(start_time, datetime.datetime.now())} milliseconds")
