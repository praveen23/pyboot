from urllib import request
import boto3
import botocore.exceptions
from pyboot2.exception import InvalidInputException


class AmazonS3Client(object):
    def __init__(self, access_key_id: str = None, access_key_secret: str = None, region_name: str = None):
        self.__client = boto3.client(
            's3', aws_access_key_id=access_key_id, aws_secret_access_key=access_key_secret,
            region_name=region_name)

    def copy(self, bucket_name: str, src_key: str, target_key: str, content_type: str = None,
             cache_control: str = None):
        params = {"CopySource": {'Bucket': bucket_name, 'Key': src_key}, "Bucket": bucket_name, "Key": target_key}
        if content_type: params["ContentType"] = content_type
        if cache_control: params["CacheControl"] = cache_control
        return self.__client.copy_object(**params)

    def copy_from_url(self, bucket_name: str, src_url: str, key: str, content_type: str = None,
                      cache_control: str = None):
        if not src_url: raise InvalidInputException("No source url found")
        response = request.urlopen(src_url)
        return self.put_str(bucket_name, key, response.read(), content_type, cache_control)

    def get_str(self, bucket_name: str, key: str):
        response = self.__client.get_object(Bucket=bucket_name, Key=key)
        if response and "Body" in response:
            return response["Body"].read(), response

    def get_stream(self, bucket_name: str, key: str):
        response = self.__client.get_object(Bucket=bucket_name, Key=key)
        if response and "Body" in response:
            return response["Body"], response

    def download_to_stream(self, bucket_name: str, key: str, stream):
        return self.__client.download_fileobj(bucket_name, key, stream)

    def download_to_file(self, bucket_name: str, key: str, filename: str):
        return self.__client.download_file(bucket_name, key, filename)

    def put_str(self, bucket_name: str, key: str, data: str, content_type: str = None, cache_control: str = None):
        params = {"Bucket": bucket_name, "Key": key, "Body": data}
        if content_type: params["ContentType"] = content_type
        if cache_control: params["CacheControl"] = cache_control
        return self.__client.put_object(**params)

    def put_stream(self, bucket_name: str, key: str, stream, content_type: str = None, cache_control: str = None):
        params = {"Bucket": bucket_name, "Key": key, "Body": stream}
        if content_type: params["ContentType"] = content_type
        if cache_control: params["CacheControl"] = cache_control
        return self.__client.put_object(**params)

    def delete(self, bucket_name: str, key: str):
        return self.__client.delete_object(Bucket=bucket_name, Key=key)

    def delete_all(self, bucket_name: str, keys: list):
        delete = {'Objects': [{'Key': key} for key in keys]}
        return self.__client.delete_objects(Bucket=bucket_name, Delete=delete)

    def exists(self, bucket_name: str, key: str):
        try:
            response = self.__client.head_object(Bucket=bucket_name, Key=key)
            return not ("DeleteMarker" in response and response["DeleteMarker"])
        except botocore.exceptions.ClientError as e:
            if e.response['Error']['Code'] == "404":
                return False
            else:
                raise

    def head(self, bucket_name: str, key: str):
        return self.__client.head_object(Bucket=bucket_name, Key=key)
