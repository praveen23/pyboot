import sentry_sdk
from sentry_sdk.integrations.flask import FlaskIntegration
from pyboot.model import Singleton
import logging


class SentryClient(object, metaclass=Singleton):
    instance = None

    def __init__(self, dsn_key, service_name, enabled=False):
        if enabled:
            logging.info("Initializing Sentry Client for {0}".format(service_name))
            sentry_sdk.init(
                dsn=dsn_key,
                integrations=[FlaskIntegration()]
            )
        else:
            logging.info("Sentry Client Disabled for {0}".format(service_name))
