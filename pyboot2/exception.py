class AppException(Exception):
    def __init__(self, message="Application error", code=None):
        super().__init__(message)
        self.code = code


class NotFoundException(AppException):
    def __init__(self, message="Not found", code=404):
        super().__init__(message=message, code=code)


class EmptyValueException(AppException):
    def __init__(self, message="Empty value", code=400):
        super().__init__(message=message, code=code)


class InvalidValueException(AppException):
    def __init__(self, message="Invalid value", code=400):
        super().__init__(message=message, code=code)


class InvalidInputException(AppException):
    def __init__(self, message="Invalid value", code=400):
        super().__init__(message=message, code=code)


class DuplicateValueException(AppException):
    def __init__(self, message="Duplicate value", code=409):
        super().__init__(message=message, code=code)


class InvalidStateException(AppException):
    def __init__(self, message="Invalid states", code=409):
        super().__init__(message=message, code=code)


class AuthFailedException(AppException):
    def __init__(self, message="Authentication failed", code=401):
        super().__init__(message=message, code=code)


class AccessDeniedException(AppException):
    def __init__(self, message="Access denied", code=403):
        super().__init__(message=message, code=code)


class HttpError(AppException):
    def __init__(self, message="Http error", code=500):
        super().__init__(message=message, code=code)
