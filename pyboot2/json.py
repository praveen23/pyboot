import datetime
import json

import decimal

from pyboot2.core import DictSerializable
from pyboot2.util import DateTimeUtil, DateUtil, TimeUtil


def __to_dict_deep(obj):
    if isinstance(obj, bytes):
        return obj.decode("utf-8")
    elif issubclass(obj.__class__, DictSerializable):
        return obj.to_dict_deep()
    elif isinstance(obj, datetime.datetime):
        return DateTimeUtil.dt_to_iso(obj)
    elif isinstance(obj, datetime.date):
        return DateUtil.date_to_iso(obj)
    elif isinstance(obj, datetime.time):
        return TimeUtil.time_to_iso(obj)
    elif isinstance(obj, decimal.Decimal):
        return float(obj)
    else:
        raise TypeError("Unable to serialize type: {type}".format(type=type(obj)))


def dump_json(obj) -> str:
    return json.dumps(obj, default=__to_dict_deep)


def load_json(obj_str: str):
    return json.loads(obj_str)
