from flask import Response
from pyboot2.values import CONTENT_TYPE_JSON
from pyboot2.json import dump_json


def json_response(obj, status=200, content_type=CONTENT_TYPE_JSON):
    return Response(response=dump_json(obj), status=status, mimetype=content_type)
