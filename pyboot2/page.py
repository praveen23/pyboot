from pyboot2.core import DictSerializable


class Page(DictSerializable):
    def __init__(self):
        self.start = 0
        self.count = 0
        self.item_count = 0
        self.total_count = 0
        self.items = []

        self.is_prev_page = False
        self.prev_page_start = None
        self.prev_page_count = None

        self.is_next_page = False
        self.next_page_start = None
        self.next_page_count = None

    def gen_page_data(self, start: int, count: int):
        if start > 0:
            self.is_prev_page = True
            if start >= count:
                self.prev_page_start = start - count
                self.prev_page_count = count
            else:
                self.prev_page_start = 0
                self.prev_page_count = start
        else:
            self.is_prev_page = False

        if len(self.items) > count:
            self.is_next_page = True
            if start + count <= self.total_count - count:
                self.next_page_start = start + count
                self.next_page_count = count
            else:
                self.next_page_start = start + count
                self.next_page_count = self.total_count - self.next_page_start
            self.items = self.items[:-1]
        else:
            self.is_next_page = False
        self.item_count = len(self.items)
        self.start = start
        self.count = count

    def to_dict_deep(self):
        page_dict = super().to_dict_deep()
        page_dict["start"] = self.start
        page_dict["count"] = self.count
        page_dict["item_count"] = self.item_count
        page_dict["total_count"] = self.total_count
        page_dict["items"] = [item.to_dict_deep() for item in self.items]
        page_dict["is_prev_page"] = self.is_prev_page
        page_dict["prev_page_start"] = self.prev_page_start
        page_dict["prev_page_count"] = self.prev_page_count
        page_dict["is_next_page"] = self.is_next_page
        page_dict["next_page_start"] = self.next_page_start
        page_dict["next_page_count"] = self.next_page_count
        return page_dict
