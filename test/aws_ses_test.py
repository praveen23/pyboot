from unittest import TestCase

from pyboot2.aws.ses import AmazonSESClient, BODY_TYPE_PLAIN, AmazonSESConf
from pyboot2.logging import LoggingConf


class AmazonSESClientTest(TestCase):
    def test_to_dict_deep(self):
        LoggingConf("conf/logging.yaml").init()

        ses_conf = AmazonSESConf("AKIAI7LD6RBBPX7MPQFQ", "bI1XNL9LuiaKwwgaXRubxNExdoWeMRJixxnFbQ1f", "us-east-1", True)
        with ses_conf.get_client() as client:
            client.send_email(sender="Rajeev Sharma <care@pankhi.com>",
                              to_addresses=["Rajeev Sharma <rajeev1982@gmail.com>"],
                              subject="This is test subject",
                              body="This is body", body_type=BODY_TYPE_PLAIN,
                              reply_addresses="Pankhi Care <care@pankhi.com>",
                              cc_addresses=["Iva Sharma <ivasharma0107@gmail.com>"],
                              bcc_addresses=["Sheena The Bot <sheenathebot@gmail.com>"],
                              attachments=[{
                                  "type": "filename",
                                  "filename": "/Users/rajeev/Downloads/invoice.pdf",
                                  "name": "Invoice.pdf"
                              }])
